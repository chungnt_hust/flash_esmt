/*
 * en25qhxx.c
 *
 *  Created on: Nov 19, 2020
 *      Author: chungnt@epi-tech.com.vn
 */
/**** include: en25qhxx.h ****/
#include "en25qhxx.h"

static uint8_t writeEN     = 0x06; // Write Enable (WREN)
static uint8_t writeDIS    = 0x04; // Write Disable (WRDI)
static uint8_t readDAT     = 0x03; // Read Data with 3 bytes address default
static uint8_t writePage   = 0x02; // Page Program (PP) with 3 bytes address default
static uint8_t sector4k    = 0x20; // Sector Erase (SE) with 3 bytes address default
static uint8_t block32k    = 0x52; // 32K Half Block Erase (HBE) with 3 bytes address default
static uint8_t block64k    = 0xD8; // 64K Block Erase (BE) with 3 bytes address default
static uint8_t chipEra     = 0x60; // Chip Erase (CE) 
static uint8_t rSR1        = 0x05; // Read Status Register (RDSR)
static uint8_t wSR1        = 0x01; // Write Status Register (WRSR)
static uint8_t rSR2        = 0x09; // Read Status Register 2 (RDSR2)
static uint8_t rSR3        = 0x95; // Read Status Register 3 (RDSR3)
static uint8_t wSR3        = 0xC0; // Write Status Register 3 (WRSR3)
static uint8_t rSR4        = 0x85; // Read Status Register 4 (RDSR4)
static uint8_t wSR4        = 0xC1; // Write Status Register 4 (WRSR4)
  
#define pinCS_SET     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET) // Lenh Set chan CS = 1;
#define pinCS_RESET   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET)  // Lenh Set chan CS = 0;

SPI_HandleTypeDef *SPI = &hspi1;
// Ham gui lenh cho phep ghi 
static void EN25QHXX_writeEnable()
{
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &writeEN,1,10);
	pinCS_SET;
}

// Ham gui lenh ko cho phep ghi
static void EN25QHXX_writeDisable()
{
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &writeDIS,   1, 10);
	pinCS_SET;
}

// Ham doc du lieu
void EN25QHXX_readData(uint32_t add, void *rx_DATA, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &readDAT, 1, 10);
	HAL_SPI_Transmit(SPI, arr,  3, 30);
	
	p = (uint8_t*)rx_DATA;
	HAL_SPI_Receive(SPI, p, len, len*10);
	
	pinCS_SET;
	//HAL_Delay(100);
}

static void EN25QHXX_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &writePage, 1, 10);
	HAL_SPI_Transmit(SPI, arr,   3, 30);
	
	HAL_SPI_Transmit(SPI, tx_DATA, len, len*10);
	
	pinCS_SET;
	EN25QHXX_writeDisable();
	HAL_Delay(100);
}

void EN25QHXX_writeData(uint32_t add, void *tx_DATA, uint32_t len)
{
	uint32_t offsetAdd;
	uint32_t luongDuLieuPhaiGhi;
	uint32_t luongDuLieuDaGhi = 0;
	
	while(len)
	{
		offsetAdd = add%256;
		if(offsetAdd + len > 256) luongDuLieuPhaiGhi = 256 - offsetAdd;
		else luongDuLieuPhaiGhi = len;
			
		uint8_t *p = (uint8_t*)tx_DATA;
		EN25QHXX_writePage(add, (uint8_t*)&p[luongDuLieuDaGhi], luongDuLieuPhaiGhi);
		add += luongDuLieuPhaiGhi;
		luongDuLieuDaGhi += luongDuLieuPhaiGhi;
		len -= luongDuLieuPhaiGhi;		
	}
}

void EN25QHXX_erase4k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &sector4k, 1, 10);
	HAL_SPI_Transmit(SPI, arr,   3, 30);
	pinCS_SET;
	EN25QHXX_writeDisable();
	HAL_Delay(100);
}

void EN25QHXX_erase32k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &block32k, 1, 10);
	HAL_SPI_Transmit(SPI, arr,   3, 30);
	pinCS_SET;
	EN25QHXX_writeDisable();
	HAL_Delay(500);
}

void EN25QHXX_erase64k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &block64k, 1, 10);
	HAL_SPI_Transmit(SPI, arr,   3, 30);
	pinCS_SET;
	EN25QHXX_writeDisable();
	HAL_Delay(1000);
}

void EN25QHXX_eraseChip(void)
{
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &chipEra,   1, 10);
	pinCS_SET;
	EN25QHXX_writeDisable();
	HAL_Delay(2000);
}

uint8_t EN25QHXX_readSR1(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &rSR1, 1, 10);
	HAL_SPI_Receive(SPI, &value, 1, 10);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t EN25QHXX_readSR2(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &rSR2, 1, 10);
	HAL_SPI_Receive(SPI, &value, 1, 10);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t EN25QHXX_readSR3(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &rSR3, 1, 10);
	HAL_SPI_Receive(SPI, &value, 1, 10);
	value = (value>>bitIndex)&0x01;
	return value;
}

void EN25QHXX_writeSR1(uint8_t value)
{
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &wSR1, 1, 10);
	HAL_SPI_Transmit(SPI, &value, 1, 10);
	pinCS_SET;
	EN25QHXX_writeDisable();
}

void EN25QHXX_writeSR2(uint8_t value)
{
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &wSR2, 1, 10);
	HAL_SPI_Transmit(SPI, &value, 1, 10);
	pinCS_SET;
	EN25QHXX_writeDisable();
}

void EN25QHXX_writeSR3(uint8_t value)
{
	EN25QHXX_writeEnable();
	pinCS_RESET;
	HAL_SPI_Transmit(SPI, &wSR3, 1, 10);
	HAL_SPI_Transmit(SPI, &value, 1, 10);
	pinCS_SET;
	EN25QHXX_writeDisable();
}

typedef struct
{
	uint8_t x1;
	uint16_t x2;
} dataType_t;
dataType_t tx[258], rx[258];

void init(void)
{
	for(uint16_t i = 0; i < 256; i++) 
	{
		tx[i].x1 = i;
		tx[i].x2 = 1000;
	}
	tx[256].x1 = tx[257].x1 = 123;
	tx[256].x2 = tx[257].x2 = 1000;
}

void EN25QHXX_selftest(void)
{
	init();
	EN25QHXX_erase32k(0x00);
	EN25QHXX_writeData(0x00, (uint8_t*)tx, sizeof(tx));
	EN25QHXX_readData(0x00, (uint8_t*)rx, sizeof(rx));
}
/****************/













