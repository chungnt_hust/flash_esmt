/*
 * en25qhxx.h
 *
 *  Created on: Nov 19, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
*/
#ifndef  __EN25QHXX_H__
#define  __EN25QHXX_H__


void EN25QHXX_readData(uint32_t add, void *rx_DATA, uint32_t len);
void EN25QHXX_writeData(uint32_t add, void *tx_DATA, uint32_t len);
void EN25QHXX_erase4k(uint32_t add);
void EN25QHXX_erase32k(uint32_t add);
void EN25QHXX_erase64k(uint32_t add);
void EN25QHXX_eraseChip(void);
uint8_t EN25QHXX_readSR1(uint8_t bitIndex);
uint8_t EN25QHXX_readSR2(uint8_t bitIndex);
uint8_t EN25QHXX_readSR3(uint8_t bitIndex);
void EN25QHXX_writeSR1(uint8_t value);
void EN25QHXX_writeSR2(uint8_t value);
void EN25QHXX_writeSR3(uint8_t value);

void EN25QHXX_selftest(void);
#endif
